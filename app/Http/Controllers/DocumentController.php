<?php

namespace App\Http\Controllers;

use App\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use File;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return 123;
        $this->validate($request,[
            'student_id' => 'required',
            'birth_certificate' => 'required|mimes:jpeg,pdf,png|max:2000',
            'documents.*' => 'nullable||mimes:jpeg,pdf,png|max:2000'
        ]);

        $path = $request->file('birth_certificate')->store('public/documents');

        $document = new Document();

        $document->student_id = $request->student_id;
        $document->storage_path = $path;
        $document->save();
        unset($document);

        if(count($request->documents)>0)
        {
            foreach($request->documents as $doc)
            {
                $path = $request->file('doc')->store('public/documents');

                $document = new Document();

                $document->student_id = $request->student_id;
                $document->storage_path = $path;
                $document->save();
                unset($document);
            }
        }

        return ['status'=>200, 'message'=>'document saved successfully'];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function show(Document $document)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function edit(Document $document)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Document $document)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function destroy(Document $document)
    {
        //
    }
}
