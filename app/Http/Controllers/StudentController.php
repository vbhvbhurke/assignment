<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;
use App\Http\Requests\StoreStudent;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $studnet = Student::all();
        return $studnet;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStudent $request)
    {
        Student::create($request->all());
        return [
            'message' => 'student created successfully',
            'status'    => 200
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = Student::find($id);
        if($student)
        {
            return $student;
        }
        else
        {
            return ["status"=>400, "message"=>"student does not exist"];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(StoreStudent $request, $id)
    {
        $student = Student::find($id);
        if($student)
        {
            Student::update($request->all());
            return ["status"=>200, "message"=>"record updated successfully"];
        }
        else
        {
            return ["status"=>400, "message"=>"student does not exist"];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $student = Student::find($id);
            $student->delete();
            return ['status'=>200, 'message'=>'student deleted successfully'];
        } catch (\Throwable $th) {
            //throw $th;
            return ['status'=>400, 'message'=>'something went wrong'];

        }

    }
}
