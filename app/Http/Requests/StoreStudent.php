<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreStudent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name'  => 'required',
            'parent_name' => 'required',
            'email'       => 'required|email',
            'mobile'      => 'digits:10|required',
            'standard'    => 'required|max:10',
            'course'      => 'required|max:50'
        ];
    }
}
